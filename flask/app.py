import os
import json
import requests
import urllib3
import math

from flask import Flask
from flask import Flask, render_template, request, jsonify

from urllib3.exceptions import InsecureRequestWarning

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def Welcome():
	return render_template('index.htm')

@app.route('/score', methods=['GET', 'POST'])
def process_form_data():
	# Get the form data - result will contian all elements from the HTML form that was just submitted
	result = request.form

	payload_scoring = {"values": [[result["property_state"], result["distance_to_centre"], result["distance_to_metro"], result["mts2"]]]}

	"""
	Change the following Headers to work with own WSL

	"""
	headers = {
		'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwicGFja2FnZU5hbWUiOiJtYSIsInBhY2thZ2VSb3V0ZSI6Im1hZHJpZCIsImlhdCI6MTU2OTU4MjQ0OX0.WWQuIQdhF8qy_ixqkivM3xCDLz81U3JLQbS_Kfh3zHi-J72ff-b1mOlltmIA1cedt5zpoIxbecIldQdhxPz3QNcN9nLuHc01Gy2UZrIK2Jd_uQlZj8uqe18ziy47bkMRrXNhufk1SWbXB6-6jYiGoMvNQILiowOvENxJlK1xR9PqkQN7FlNdQKnH-JhcyxLzDxQCKARL_A39C5Jg7hPIAZr2QDZjn6u3k6cqaIhdkUMegAEdHnM_9GeQ9CLrVmA44bN69vtZbchMkjWDJ1L8580lqMsLFtZw_UjEOqmkgQS_-YRoLIXp30woz2HTFX45RD05_LMAp-e1HLdjIZpi0Q',
		'Cache-Control': 'no-cache',
		'Content-Type': 'application/json',
	}

	
	json_data = {"args":{"input_json":[{"property_state":result["property_state"],"distance_to_centre":result["distance_to_centre"],"distance_to_metro":result["distance_to_metro"],"mts2": result["mts2"]}]}}


	"""
	Change the following URL to work with own WSL
	"""
	response = requests.post('https://secure.bluedemos.com:17962/dmodel/v1/madrid/pyscript/madrid/score', headers=headers, json=json_data, verify=False)	#
	
	
	json_response = response.json()
	print(json_response)

	json_response['exp_values'] = math.exp(json_response['result']['predictions'][0]) 

	return jsonify( json_response ) 
	
port = os.getenv('VCAP_APP_PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
