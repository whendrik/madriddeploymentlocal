# Madrid Deployment with WSLocal

Within the `flask` directory:

- Modify the `app.py` to make sure `headers` and `requests.post()` point to your token/url combination
- Install requirements with pip, `pip install -r requirements.txt`
- run with `python app.py`
- Go to url http://127.0.0.1:5000/